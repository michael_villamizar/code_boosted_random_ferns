/* 

 mex_HOG
 Michael Villamizar
 mvillami@iri.upc.edu
 Institut de Robòtica i Informàtica Industrial CSIC-UPC
 2009

 Description:
	This function computes the histogram of oriented gradients -HOG- over the input image.

 Input:
	prhs[0] -> image
	prhs[1] -> num. gradient orientation bins
	prhs[2] -> HOG cell size		

 Output:
	plhs[0] -> HOG

 Requirements:
	- The input image must be in gray scales and normalized.	
 
*/


#include <math.h>
#include "mex.h"
#include <stdio.h>

// variables
#define PI 3.14159265

// main function
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) { 

	// check
	if(nrhs!=3) 
  		mexErrMsgTxt("Three inputs required: 1. image 2. num. orientation bins 3. cell size");
  	if(nlhs!=1) 
    	mexErrMsgTxt("One output required: HOG");

	// copy input pointers
	mxArray *data0 = (mxArray *)prhs[0];
	mxArray *data1 = (mxArray *)prhs[1];
	mxArray *data2 = (mxArray *)prhs[2];

	// data pointers
	double *img = mxGetPr(data0);					// image -gray scale-
	int numBins = (int)mxGetScalar(data1);		// num. gradient orientation bins
	int cell    = (int)mxGetScalar(data2);		// cell size -pixels-

	// num. image dimensions
	int numDims = mxGetNumberOfDimensions(data0);

	// check
	if (numDims!=2)
		mexErrMsgTxt("The input image must be in gray scales");

	// image size	
	const int *imgSize = mxGetDimensions(data0);
	int sy = imgSize[0];
	int sx = imgSize[1];

	// angle increment
	double angInc = (double)PI/(double)numBins;	

	// HOG size
	int hy = ceil((double)sy/cell);
	int hx = ceil((double)sx/cell);

	// output -HOG-
	int out[3];
	out[0] = hy;
	out[1] = hx;
	out[2] = numBins;
	plhs[0]= mxCreateNumericArray(3, out, mxDOUBLE_CLASS, mxREAL);
	double *HOG = (double *)mxGetPr(plhs[0]);

	// variables
	double dy,dx,mag,ang;
	int bina,binx,biny;
	
	// scanning
	for (int x=1; x<sx-1; x++){
		for (int y=1; y<sy-1; y++){

			// derivatives
			dy = *(img + (y+1) + x*sy) - *(img + (y-1) + x*sy);
			dx = *(img + y + (x+1)*sy) - *(img + y + (x-1)*sy);

			// gradient magnitude
			//mag = fabs(dx) + fabs(dy);
			mag = sqrt(dx*dx + dy*dy);
			
			// reject small gradients
			if (mag<0.00001){ continue; }

			// gradient angle
			ang = atan2(dy,dx);

			// 180 degrees gradient format (0-180) "unsigned format"	
			if (ang<0){ ang = fabs(ang + PI); }
				
			// orientation bin
			bina = (int)floor(ang/angInc);

			// spatial bins
			binx = (int)floor(double(x)/cell);
			biny = (int)floor(double(y)/cell);
			
			// check 
			if (binx>=0 && binx<hx && biny>=0 && biny<hy && bina>=0 && bina<numBins){

				// HOG
				*(HOG + biny + binx*hy + bina*hy*hx) +=  mag;

			}

		}
	}
}



