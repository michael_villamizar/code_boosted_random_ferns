%% random ferns -RFs-
% This function computes the random ferns. Each fern corresponds to a set 
% of binary features, and each feature is a -signed- comparision between 
% two points (e.g pixels intensities). This function computes -at random- 
% the location (i.e y, x, feature channel) of these points.
function output = fun_random_ferns()

% load/compute the random ferns
try

    % load previous random ferns
    ferns = fun_data_load('./variables/','ferns.mat');
        
catch ME

    % parameters
    prms = fun_parameters();  % program parameters                                         
    numFerns = prms.ferns.numFerns;  % num. random ferns                             
    numFeats = prms.ferns.numFeats;  % num. binary features per fern                              
    fernSize = prms.ferns.fernSize;  % fern size -spatial squared size-
    
    % num. image feature channels
    numChans = fun_image_feature_channels();
    
    % allocate memory
    data = zeros(numFeats,6,numFerns);
    
    % random ferns
    for iterFern = 1:numFerns
            
        % Binary feature: feature value comparison
        % between two random points -A & B-. The 
        % points (e.g poixels) are chosen at random
        ya = floor(rand(numFeats,1)*fernSize);  % point A: location y
        xa = floor(rand(numFeats,1)*fernSize);  % point A: location x
        ca = floor(rand(numFeats,1)*numChans);  % point A: feature channel
        yb = floor(rand(numFeats,1)*fernSize);  % point B: location y    
        xb = floor(rand(numFeats,1)*fernSize);  % point B: location x
        cb = floor(rand(numFeats,1)*numChans);  % point B: feature channel
       
        % check fern values
        ya = min(fernSize-1,max(0,ya));
        xa = min(fernSize-1,max(0,xa));
        ca = min(numChans-1,max(0,ca));
        yb = min(fernSize-1,max(0,yb));
        xb = min(fernSize-1,max(0,xb));
        cb = min(numChans-1,max(0,cb));
        
        % fern data
        % data(:,:,iterFern) = [ya,xa,ca,yb,xb,ca];  % equal feature channels
        data(:,:,iterFern) = [ya,xa,ca,yb,xb,cb];    % mixed feature channels
        
    end
    
    % random ferns
    ferns.data = data;  % ferns data                                          
    ferns.numFerns = numFerns;  % num. random ferns                              
    ferns.numFeats = numFeats;  % num. binary features                            
    ferns.fernSize = fernSize;  % fern size                              
    ferns.numChans = numChans;  % num. feature channels -fern depth-                       
    
    % save
    fun_data_save(ferns,'./variables/','ferns.mat');

end 

% message
fun_messages('random ferns -RFs-','process');
fun_messages(sprintf('num. ferns: %d',ferns.numFerns),'information');
fun_messages(sprintf('num. features: %d',ferns.numFeats),'information');
fun_messages(sprintf('size: [%d %d]',ferns.fernSize,ferns.fernSize),'information');
fun_messages(sprintf('num. channels: %d',ferns.numChans),'information');

% output
output = ferns;
end
