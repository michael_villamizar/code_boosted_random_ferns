%% Classifier: Boosted Random Ferns -BRFs-
% This function computes the object classifier using the input training
% samples and the set of random ferns
function output = fun_classifier_brfs(samples,ferns)
if (nargin<2),fun_messages('incorrect parameters','error'); end

% parameters
prms = fun_parameters();                    % program parameters
omega = prms.classifier.omega;              % threshold for outlier rejection
numWCs = prms.classifier.numWCs;            % num. weak classifiers (WCs)
objSize = prms.classifier.objSize;          % object size
numFeats = prms.ferns.numFeats;             % num. binary features per fern
numBins = 2^numFeats;                       % num. histogram bins -fern outputs-
numPosSamples = samples.numPosSamples;      % num. positive image samples
numNegSamples = samples.numNegSamples;      % num. negative image samples

% Potential weak classifiers (PWC): this computes the pool of all possible 
% weak classifiers used to compute the object classifier. Each potential 
% weak classifier corresponds to a random fern tested in a particular image 
% location [y,x]. Boosting selects the most discriminative weak classifiers
% (WCs) from this pool of weak classifiers (PWCs)
PWCs = fun_potential_weak_classifiers();
numPWCs = size(PWCs,1);

% Fern maps: the set of random ferns are convolved over the input samples
% in order to compute the outputs of ferns associated with the potential 
% weak classifiers (PWCs). This also alliviates the computational cost
posFernMaps = mex_fern_maps(samples.positives,ferns.data,ferns.fernSize);
negFernMaps = mex_fern_maps(samples.negatives,ferns.data,ferns.fernSize);

% messages
fun_messages('classifier: Boosted Random Ferns (BRFs)','process');
fun_messages(sprintf('num. weak classifiers -> %d',numWCs),'information');
fun_messages(sprintf('num. potential weak classifiers -> %d',numPWCs),'information');

% test potential weak classifiers over images -fern maps-
posOutputs = fun_test_potential_weak_classifiers(PWCs,posFernMaps);
negOutputs = fun_test_potential_weak_classifiers(PWCs,negFernMaps);

% num. input samples
numSamples = numPosSamples + numNegSamples;

% initial sample weights
weights = (1/numSamples)*ones(1,numSamples);

% sample labels: +1/-1
labels = [ones(1,numPosSamples), -1*ones(1,numNegSamples)];

% variables
timSum = 0;     % times

% allocate
WCs = zeros(numWCs,3);                          % weak classifiers (WCs) data
hstms = zeros(numWCs,numBins);                  % weak classifiers probabilities -histograms-
indexes = zeros(1,numWCs);                      % selected weak classifiers indexes    
posDetMaps = zeros(numWCs,numPosSamples);       % positive detection map
negDetMaps = zeros(numWCs,numNegSamples);       % negative detection map

% boosting 
for iter = 1:numWCs
    
    % times
    tic;
    
    % weights -positive and negative-
    posWeights = weights(1,1:numPosSamples);
    negWeights = weights(1,1+numPosSamples:end);
    
    % Weak learner: this function chooses the most discriminative weak classifier (WC)
    % from the pool of potential weak classifiers (PWCs). This is done in accordance to 
    % the current samples weights and the classification error
    WC = fun_weak_learner(PWCs,posOutputs,negOutputs,posWeights,negWeights,numBins,indexes);
    
    % test the selected weak classifier
    posDetVals = fun_test_weak_classifier(WC,posOutputs);
    negDetVals = fun_test_weak_classifier(WC,negOutputs);
    
    % update sample weights
    weights = weights.*(exp(-1*labels.*[posDetVals,negDetVals]));
    
    % threshold for outliers rejection
    weights = min(omega,weights);
   
    % weight normalization
    weights = weights./sum(weights(:));      
    
    % update index list
    indexes(1,iter) = WC.index;
    
    % save -chosen -weak classifier data
    WCs(iter,:) = WC.data;              % weak classifier data
    hstms(iter,:) = WC.hstm;            % weak classifier probabilities
    posDetMaps(iter,:) = posDetVals;        % positive detection maps
    negDetMaps(iter,:) = negDetVals;        % negative detection maps
    
    % times
    timSum = timSum + toc;
    timAve = timSum/iter;  
    timRes = (numWCs-iter)*timAve/60;
   
    % message
    if(mod(iter,25)==0), fun_messages(sprintf('weak classifier -> %d : time -> %.3f min',iter,timRes),'information'); end
    
end

% detection map
fun_detection_map(posDetMaps,negDetMaps);

% classifier: brfs
clfr.prms = prms;           % program parameters
clfr.WCs = WCs;             % weak classifiers data
clfr.hstms = hstms;         % weak classifier probabilities
clfr.ferns = ferns;         % random ferns
clfr.numWCs = numWCs;       % num. weak classifiers
clfr.objSize = objSize;     % object size

% output
output = clfr;
end

%% Weak learner
% This function chooses the most discriminative weak classifier (WC) from the 
% pool of potential weak classifiers (PWCs). This is done in accordance to 
% the current samples weights and the classification error
function output = fun_weak_learner(PWCs,posOutputs,negOutputs,posWeights,negWeights,numBins,indexes)
if (nargin<7),fun_messages('incorrect parameters','error'); end

% parameters
prms = fun_parameters();        % program parameters
eps = prms.classifier.eps;      % epsilon value

% fern output histograms
posHstms = fun_hstms(posWeights,posOutputs,numBins);
negHstms = fun_hstms(negWeights,negOutputs,numBins);

% num. potential weak classifiers
numPWCs = size(posOutputs,1);

% max. distance between distributions
Qmax = inf;

% test potential weak classifiers (PWCs)
for iterPWC = 1:numPWCs

    % positive and negative histograms
    posHstm(1,:) = posHstms(iterPWC,:);     
    negHstm(1,:) = negHstms(iterPWC,:);
    
    % normalization
    posHstm = fun_normalize(posHstm);
    negHstm = fun_normalize(negHstm);
       
    % check
    if (sum(posHstm(:))<=0), continue; end
    if (sum(negHstm(:))<=0), continue; end
    
    % distance between distributions
    Q = 2*sum(sqrt(posHstm.*negHstm),2);
    
    % min. distance
    if (Q<Qmax)
        
        % check repeated weak classifiers
        if (sum(indexes==iterPWC,2)==0)
                
            % update
            Qmax = Q;
                
            % log. ratio of fern outputs
            logHstm = 0.5*log((posHstm + eps)./(negHstm + eps));
            
            % weak classifier -WC-
            WC.hstm = logHstm;             % weak classifier histogram
            WC.data = PWCs(iterPWC,:);     % weak classifier data
            WC.index = iterPWC;            % weak classifier index
           
        end
    end
end

% output
output = WC;
end

%% Ferns histograms
function output = fun_hstms(weights,fernOutps,numBins)
if (nargin<3),fun_messages('incorrect parameters','error'); end

% num. potential weak classifiers and samples
[numPWCs,numSamples] = size(fernOutps);

% allocate
hstms = zeros(numPWCs,numBins);

% samples
for iterWC = 1:numPWCs
    for iterSample = 1:numSamples
        
        % fern output
        z = fernOutps(iterWC,iterSample);
        
        % update histogram
        hstms(iterWC,z) = hstms(iterWC,z) + weights(iterSample);
        
    end
end

% output
output = hstms;
end

%% Test weak classifier
function output = fun_test_weak_classifier(WC,fernOutps)
if (nargin<2),fun_messages('incorrect parameters','error'); end

% num. samples
numSamples = size(fernOutps,2);

% allocate
detVals = zeros(1,numSamples);

% samples
for iterSample = 1:numSamples
    
    % fern output
    z = fernOutps(WC.index,iterSample);
    
    % detection values
    detVals(1,iterSample) = WC.hstm(1,z);

end

% output
output = detVals;
end

%% Normalization
% This function normalizes the input histogram to 1
function output = fun_normalize(hstm)

% normalize histogram
if (sum(hstm(:))>0), hstm = hstm./sum(hstm(:)); end

% output
output = hstm;
end

%% Detection map
% This function shows the detection performance of the computed classifier
% over the training samples. The function plots the classifier confidence 
% of weak classifiers over the samples
function fun_detection_map(posMap,negMap)
if (nargin~=2), fun_messages('incorrect input parameters','error'); end

% parameters
prms = fun_parameters();            % program parameters
fs = prms.visualization.fontSize;   % font size
lw = prms.visualization.lineWidth;  % line width

% classifier confidence
posCurve = sum(posMap,1);
negCurve = sum(negMap,1);

% show maps
figure,subplot(211),imagesc([posMap negMap]),colormap(gray);
xlabel('Detection map','fontsize',fs);
subplot(212),plot(posCurve,'g','linewidth',lw),hold on,
plot(negCurve,'r','linewidth',lw),
xlabel('Classifier confidence','fontsize',fs);
legend('positive','negative');

end






