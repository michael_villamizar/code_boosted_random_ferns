%% load images
% This funcion loads images from files. The images are scaled and proccesed 
% -feature extraction- according to the program parameters.
function output = fun_load_images(imgPath,numImgs,imgFmt,imgSize)
if (nargin~=4), fun_messages('incorrect input parameters','error'); end

% image files
imgFiles = dir([imgPath,'*',imgFmt]);

% num. image samples
numSamples = min(numImgs,size(imgFiles,1));

% check
if (numSamples==0), fun_messages('no images in this path','error'); end

% image feature channels: this sets the number of features channels 
% according to the image feature space (e.g RGB, HOG)
numChannels = fun_image_feature_channels();

% allocate
samples = zeros([imgSize,numChannels,numSamples]);

% image samples
for iterImg = 1:numSamples
    
    % current image
    img = imread([imgPath,imgFiles(iterImg).name]);
    
    % feature extraction: the current image -img- is processed to work with
    % particular features (e.g intensities, gradients). This is done in 
    % accordance to image feature space defined by the user in the program
    % parameters.
    img = fun_feature_image(img,imgSize);
        
    % save
    samples(:,:,:,iterImg) = img;
    
end

% output
output = samples;
end
