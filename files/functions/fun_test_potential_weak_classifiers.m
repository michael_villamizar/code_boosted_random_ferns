%% test the potential weak classifiers (PWCs)
% This function tests the pool containing all potential weak classifiers 
% on the input sample. To speed up this process, the feature computation 
% -fern maps- is done in advance thanks to the features -ferns- are shared 
% across weak classifiers.
function output = fun_test_potential_weak_classifiers(PWCs,fernMaps)
if (nargin~=2), fun_messages('incorrect input parameters','error'); end

% variables
numPWCs = size(PWCs,1);  % num. potential weak classifiers
numSamples = size(fernMaps,4);  % mum. input image samples

% allocate
data = zeros(numPWCs,numSamples);  % weak classifier outputs

% test each potential weak classifier
for iterPWC = 1:numPWCs
    for iterSample = 1:numSamples
        
        % PWC properties
        py = PWCs(iterPWC,1);  % fern location in y
        px = PWCs(iterPWC,2);  % fern location in x
        fi = PWCs(iterPWC,3);  % fern index
        
        % fern output
        z = fernMaps(py,px,fi,iterSample);
        
        % save
        data(iterPWC,iterSample) = z;
    end
end

% output
output = data;
end
