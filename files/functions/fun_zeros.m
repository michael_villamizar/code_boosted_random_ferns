%% zeros
% This function returns the zeros needed to fill four digits in the input 
% number.
function output = fun_zeros(number)
if (nargin~=1), fun_messages('incorrect input parameters','error'); end

% zeros
if (number<10000), zrs = ''; end
if (number<1000), zrs = '0'; end
if (number<100), zrs = '00'; end
if (number<10), zrs = '000'; end

% output
output = zrs;
end