%% show  the classifier
% This function shows the computed classifier. This shows the density map
% of the weak classifiers -ferns- selected during the training phase.
function fun_show_classifier(clfr)
if (nargin~=1), fun_messages('incorrect input parameters','error'); end

% parameters
prms = fun_parameters();  % program parameters
numWCs = clfr.prms.classifier.numWCs;  % num. weak classifiers
objSize = clfr.prms.classifier.objSize;  % object size
numFerns = clfr.prms.ferns.numFerns;  % num. ferns
fernSize = clfr.prms.ferns.fernSize;  % fern size
fontSize = prms.visualization.fontSize;  % font size
lineWidth = prms.visualization.lineWidth;  % line width

% allocation
img = zeros([objSize(1:2),3]);  % image
map = zeros(size(img,1),size(img,2));  % feature map
hstm = zeros(1,numFerns);  % ferns histogram

% figure
figure,subplot(211),imagesc(img),hold on
title('Spatial Fern Layout','fontsize',fontSize);
xlabel('Fern locations ','fontsize',fontSize);

% show fern location
for iter = 1:numWCs
   
   % weak classifier properties: fern location and index 
   fy = clfr.WCs(iter,1);
   fx = clfr.WCs(iter,2);
   fi = clfr.WCs(iter,3);
   
   % update fern histogram
   hstm(1,fi) = hstm(1,fi) + 1;
   
   % area color
   color = fun_colors(fi);
   
   % fern area
   subplot(211),rectangle('position',[fx,fy,fernSize,fernSize],...
       'edgecolor',color,'linewidth',lineWidth),hold on
  
   % update areas
   for iterY = fy:fy+fernSize-1
        for iterX = fx:fx+fernSize-1
            map(iterY,iterX) = map(iterY,iterX) + 1;
        end
   end
end

% normalization
map = map./max(map(:));

% distribution map
subplot(212),imagesc(map);
xlabel('Fern density map','fontsize',fontSize);

% fern distribution
figure,bar(hstm),xlabel('Shared Random Ferns','fontsize',fontSize);
ylabel('# weak classifiers','fontsize',fontSize);
title('Fern distribution','fontsize',fontSize);

end




