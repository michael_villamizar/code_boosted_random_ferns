%% test
% This function tests the computed object classifier on test images. The 
% classifier is tested in positive and negative test images.
function fun_test(clfr)
if (nargin~=1), fun_messages('incorrect input parameters','error'); end

% messages
fun_messages('test','title');

% parameters
prms = fun_parameters();  % program parameters
posImgFmt = prms.test.posImgFmt;  % positive image format
negImgFmt = prms.test.negImgFmt;  % negative image format
numPosImgs = prms.test.numPosImgs;  % num. positive images
numNegImgs = prms.test.numNegImgs;  % num. negative images
posImgPath = prms.test.posImgPath;  % positive images path
negImgPath = prms.test.negImgPath;  % negative images path

% test positive and negative images
fun_test_images(clfr,posImgPath,numPosImgs,posImgFmt,'positives');
fun_test_images(clfr,negImgPath,numNegImgs,negImgFmt,'negatives');

end

%% test images
% This function tests the classifier over a set of images corresponding
% to a specific dataset.
function fun_test_images(clfr,imgPath,numImgs,imgFmt,text)
if (nargin~=5), fun_messages('incorrect input parameters','error'); end

% parameters
prms = fun_parameters();  % parameters    
intThr = prms.runtime.intThr;  % intersection area -overlapping- threshold
detThr = prms.runtime.detThr;  % detection threshold
detColor = prms.colors.posColor;  % detection rectangle color
detThick = prms.runtime.detThick;  % detection rectangle thickness

% load images
imgFiles = dir([imgPath,'*',imgFmt]);

% num. images
numImgs = min(numImgs,size(imgFiles,1));

% variable
timSum = 0;  % times

% messages
fun_messages(sprintf('%s images',text),'process');
fun_messages(sprintf('num. %s images -> %d',text,numImgs),'information');

% images
for iterImg = 1:numImgs
    
    % times
    tic;
    
    % current image
    name = imgFiles(iterImg).name;                         
    img = imread([imgPath,name]);
    
    % detection image 
    detImg = fun_image_color(img,'RGB');
    
    % object detection
    dets = fun_detection(img,clfr,detThr);
   
    % detection boxes and scores
    boxes = dets.boxes; 
    scores = dets.scores;
   
    % detection results
    detResults.name = name;  % image file name
    detResults.times = dets.times;  % detection times
    detResults.boxes = dets.boxes;  % detection bounding boxes
    detResults.scores = dets.scores;  % detection scores
    
    % save detection results
    fun_data_save(detResults,sprintf('./detections/%s/',text),sprintf('%s.mat',name));
    
    % non-maxima supression
    [boxes,scores] = fun_non_maxima_suppression(boxes,scores,intThr);
    
    % max. detection score
    maxScore = max(scores);
        
    % show detections
    for iterBox = 1:size(boxes,1);
        detImg = fun_draw_rectangle(detImg,boxes(iterBox,:),detColor,detThick);
    end
    
    % write detection image
    fun_image_write(detImg,'./images/detections/',text,iterImg);
    
    % times
    timSum = timSum + toc;  
    timAve = timSum/iterImg;
    timRes = (numImgs-iterImg)*timAve/60; 
    
    % message
    fun_messages(sprintf('image -> %d/%d : max. score -> %.3f : rest time -> %.3f',...
        iterImg,numImgs,maxScore,timRes),'information');
    
end
end
