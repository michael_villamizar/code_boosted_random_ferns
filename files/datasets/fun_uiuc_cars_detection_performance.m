%% UIUC Cars Dataset: Detection Performance
% This function computes the detection performance of BRFs. In detail, the
% function loads the detection results from a specific experiment (e.g 
% brfs_t1) and compute the detection curves. The detections results are
% saved at specific folder (e.g /variables/).
function fun_uiuc_cars_detection_performance()
clc,clear all,close all
    
% messages
fun_messages('UIUC Cars Dataset','presentation');
fun_messages('Detection Perfomance Curves','title');

% parameters
minThr = -1;  % min. detection threshold
maxThr = +1;  % max. detection threshold
intThr = 0.3;  % intersection -overlapping- threshold
outPath = './variables/';  % output file -results- path
detPath = './results/brfs_t1/detections/positives/';  % detection files path -experiment brfs_t1-
annPath = './images/datasets/uiuc_cars/test_images/annotations/';  % annotation files path -ground truth-
numRounds = 50;  % num. rounds -thresholds-

% threshold step
step= max(0.001,(maxThr-minThr)/(numRounds-1));
    
% detection files
detFiles = dir([detPath,'*.mat']);
    
% num. detection files
numFiles = size(detFiles,1);

% messages
fun_messages('curves computation','process');
fun_messages(sprintf('num. detection files -> %d',numFiles),'information');

% curve properties
cur_roc = zeros(1,numRounds);   % roc: false positives per image
cur_rec = zeros(1,numRounds);   % recall
cur_pre = zeros(1,numRounds);   % precision
cur_thr = zeros(1,numRounds);   % thresholds
cur_tps = zeros(1,numRounds);   % true positives
cur_fps = zeros(1,numRounds);   % false positives
cur_fns = zeros(1,numRounds);   % false negatives

% variables
count = 0;

% detection performane curves: the curves are computed
% varying the classifier threshold and measuring the 
% detection rates such as recall and precision on the
% detection results obtained during the test step.
for iterThr = minThr:step:maxThr
    
    % message
    fun_messages(sprintf('threshold -> %.03f',iterThr),'information');
    
    % rates variables
    fps = 0;  % false positives
    tps = 0;  % true positives
    fns = 0;  % false negatives
    
    % test files
    for iterFile = 1:numFiles
        
        % detection file name
        fileName = detFiles(iterFile).name;
        
        % detection data
        detData = fun_data_load(detPath,fileName);
        
        % annotation data -ground truth-
        annData = fun_data_load(annPath,[detData.name,'.mat']);
                
        % annotation data
        annBoxes = annData.boxes;  % bounding boxes -objects annotated in the image-
        numObjects = annData.numBoxes;  % num. bounding boxes -objects-
        
        % thresholding: detections over threshold
        indx = detData.scores>iterThr;
        boxes = detData.boxes(indx,:);
        scores = detData.scores(indx,:);
            
        % non-maxima suppression: overlapped detection boxes are removed
        % according to the input intersection -overlapping- threshold. 
        newBoxes = fun_non_maxima_suppression(boxes,scores,intThr);
            
        % intersection measure: This measures the degree of overlapping 
        % -intersection- between the annotation boxes and the detection
        % boxes after non-maxima suppression.
        intValues = fun_boxes_intersection(annBoxes,newBoxes,0.5);

        % classification rates
        tmp = sum(intValues.indexes,1);
        tps = tps + sum(tmp>0,2);  % true positives
        fps = fps + sum(tmp==0,2);  % false positives
        fns = fns + numObjects - sum(tmp>0,2);  % false negatives
        
    end
    
    % counter
    count = count + 1;
    
    % save curve properties
    cur_pre(1,count) = tps/(tps + fps);  % precision
    cur_rec(1,count) = tps/(tps + fns);  % recall
    cur_roc(1,count) = fps/numFiles;   % roc 
    cur_tps(1,count) = tps;  % true positives
    cur_fps(1,count) = fps;  % false positives
    cur_fns(1,count) = fns;  % false negatives
    cur_thr(1,count) = iterThr;  % threshold
    
end

% equal error rate -EER- index
[~,index] = min(abs(cur_pre(:)-cur_rec(:)));

% check empty rate values
if (cur_rec(index)==0 && cur_pre(index)==0)
    fun_messages('incorrect EER value','warning');
    [~,index] = min(abs(cur_pre(1:index-1)-cur_rec(1:index-1)));
end

% EER values
eer_rec = cur_rec(index);  % recall at EER
eer_pre = cur_pre(index);  % precision at EER
eer_tps = cur_tps(index);  % num. true positives at EER
eer_fps = cur_fps(index);  % num. false positives at EER
eer_fns = cur_fns(index);  % num. false negatives at EER
eer_thr = cur_thr(index);  % threshold to get the EER
eer_val = 0.5*(eer_rec + eer_pre);  % EER value

% check innacurate EER value
if (abs(eer_rec - eer_pre)>0.1), fun_messages('EER value is not accurate','warning'); end

% messages
fun_messages('Equal Error Rates','process');
fun_messages(sprintf('EER: value -> %0.4f',eer_val),'information');
fun_messages(sprintf('EER: recall -> %0.4f',eer_rec),'information');
fun_messages(sprintf('EER: precision -> %0.4f',eer_pre),'information');
fun_messages(sprintf('EER: true positives -> %d',eer_tps),'information');
fun_messages(sprintf('EER: false positives -> %d',eer_fps),'information');
fun_messages(sprintf('EER: false negatives -> %d',eer_fns),'information');
fun_messages(sprintf('EER: threshold -> %.4f',eer_thr),'information');

% show the precision-recall curve -PR-
figure,plot(cur_rec(:),cur_pre(:),'r-','linewidth',4),axis([0 1 0 1])
ylabel('Precision','fontsize',26), xlabel('Recall','fontsize',26);
line([0;1],[0;1],'color','k');

% show the roc curve
figure,plot(cur_roc(:),cur_rec(:),'b-','linewidth',4),axis([0 1 0 1])
xlabel('FPPI','fontsize',26), ylabel('Recall','fontsize',26);
line([0;1],[0;1],'color','k');

% detection results -curves and EER values-
curve.recall = cur_rec;  % recall curve
curve.precision = cur_pre;  % precision curve
curve.truePositive = cur_tps;  % true positives curve
curve.falsePositive = cur_fps;  % false positives curve
curve.falseNegative = cur_fns;  % false negatives curve
curve.threshold = cur_thr;  % threshold curve
curve.fppi = cur_roc;  % false positive per image curve -roc-
curve.eer.value = eer_val;  % EER value
curve.eer.recall = eer_rec;  % recall value at EER
curve.eer.precision = eer_pre;  % precision value at EER
curve.eer.threshold = eer_thr;  % threshold value at EER
curve.eer.truePositive = eer_tps;  % num. true positives at EER
curve.eer.falsePositive = eer_fps;  % num. false positives at EER
curve.eer.falseNegative = eer_fns;  % num. false negatives at EER

% save detection results
fun_data_save(curve,outPath,'PRCurve.mat');

% message
fun_messages('End','title');
end

